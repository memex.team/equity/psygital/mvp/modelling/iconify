/**
 * This program and the accompanying materials
 * are made available under the terms of the License
 * which accompanies this distribution in the file LICENSE.txt
 */
package org.archicontribs.iconify.uiProvider.elements;

import org.archicontribs.iconify.SpecializationLogger;
import org.archicontribs.iconify.Iconify;
import org.eclipse.gef.EditPart;
import org.eclipse.swt.graphics.Image;

import com.archimatetool.editor.diagram.editparts.ArchimateElementEditPart;


/**
 * Business Object UI Provider
 * 
 * @author Herve Jouin
 */
public class BusinessObjectUIProvider extends com.archimatetool.editor.ui.factory.elements.BusinessObjectUIProvider {
    private static final SpecializationLogger logger = new SpecializationLogger(BusinessObjectUIProvider.class);
    
    @Override
    public EditPart createEditPart() {
            // we override the standard method because we want our ObjectFigure class to be called
        return new ArchimateElementEditPart(org.archicontribs.iconify.uiProvider.elements.figures.ObjectFigure.class);
    }
    
    /**
     * Gets the icon image from the component's properties. If not found, return the default one.
     */
    @Override
    public Image getImage() {
    	if ( Iconify.mustReplaceIcon(this.instance) ) {
    	    Image image = Iconify.getImage(this.instance);
            if ( image != null ) {
                if ( logger.isTraceEnabled() ) logger.trace(Iconify.getFullName(this.instance)+": Displaying custom icon");
                return image;
            }
    	}
    	
        logger.trace(Iconify.getFullName(this.instance)+": Displaying default icon");        	
    	return super.getImage();
    }
}
