/**
 * This program and the accompanying materials
 * are made available under the terms of the License
 * which accompanies this distribution in the file LICENSE.txt
 */
package org.archicontribs.iconify.uiProvider.elements.figures;

import org.archicontribs.iconify.Iconify;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.text.TextFlow;

/**
 * Figure for an Application Component
 * 
 * @author Herve Jouin
 */
public class ApplicationComponentFigure extends com.archimatetool.editor.diagram.figures.elements.ApplicationComponentFigure {
    
    @Override
    protected void drawIcon(Graphics graphics) {
    	if ( Iconify.mustReplaceIcon(getDiagramModelObject()) && (Iconify.getPropertyValue(getDiagramModelObject(), "icon") != null) )
    		Iconify.drawIcon(getDiagramModelObject(), graphics, this.bounds);
    	else
    		super.drawIcon(graphics);
    }
    
   
}