/**
 * This program and the accompanying materials
 * are made available under the terms of the License
 * which accompanies this distribution in the file LICENSE.txt
 */
package org.archicontribs.iconify.uiProvider.elements.figures;

import org.archicontribs.iconify.Iconify;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.text.TextFlow;

/**
 * Representation Figure
 * 
 * @author Herve Jouin
 */
public class RepresentationFigure extends com.archimatetool.editor.diagram.figures.elements.RepresentationFigure {
	// RepresentationFigure do not have a drawIcon method

}
