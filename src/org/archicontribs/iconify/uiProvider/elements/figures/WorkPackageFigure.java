/**
 * This program and the accompanying materials
 * are made available under the terms of the License
 * which accompanies this distribution in the file LICENSE.txt
 */
package org.archicontribs.iconify.uiProvider.elements.figures;

import org.archicontribs.iconify.Iconify;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.text.TextFlow;

/**
 * Work Package Figure
 * 
 * @author Herve Jouin
 */
public class WorkPackageFigure extends com.archimatetool.editor.diagram.figures.elements.WorkPackageFigure {
	// WorkpackageFigure do not have drawIcon method !!!
	

}
