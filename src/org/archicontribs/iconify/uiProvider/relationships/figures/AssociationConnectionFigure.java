package org.archicontribs.iconify.uiProvider.relationships.figures;

import org.archicontribs.iconify.Iconify;

public class AssociationConnectionFigure extends com.archimatetool.editor.diagram.figures.connections.AssociationConnectionFigure {
    @Override
    protected void setConnectionText() {
        String labelName = null;
        
        if ( Iconify.mustReplaceLabel(getModelConnection()) )
            labelName = Iconify.getLabelName(getModelConnection());
        
        if ( labelName==null )
            getConnectionLabel().setText(getModelConnection().getArchimateRelationship().getName());
        else
            getConnectionLabel().setText(labelName);
    }
}