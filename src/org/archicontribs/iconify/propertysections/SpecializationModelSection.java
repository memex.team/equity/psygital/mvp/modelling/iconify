/**
 * This program and the accompanying materials
 * are made available under the terms of the License
 * which accompanies this distribution in the file LICENSE.txt
 */
package org.archicontribs.iconify.propertysections;

import org.archicontribs.iconify.SpecializationLogger;
import org.archicontribs.iconify.Iconify;
import org.archicontribs.iconify.SpecializationPropertyCommand;
import org.eclipse.gef.commands.CommandStack;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.ui.PlatformUI;

import com.archimatetool.editor.model.commands.NonNotifyingCompoundCommand;
import com.archimatetool.model.IArchimateModel;
import com.archimatetool.model.IArchimatePackage;

public class SpecializationModelSection extends org.archicontribs.iconify.propertysections.AbstractArchimatePropertySection {
	static final SpecializationLogger logger = new SpecializationLogger(SpecializationModelSection.class);

	IArchimateModel model;

	private Label lblIconInViewsInfo;
	private Label lblReplaceInViewsIcons;
	Button btnIconsInViewsYes;
	Button btnIconsInViewsNo;
	private Button btnIconsInViewsDefault;
	
    private Label lblIconInTreeInfo;
    private Label lblReplaceInTreeIcons;
    Button btnIconsInTreeYes;
    Button btnIconsInTreeNo;
    private Button btnIconsInTreeDefault;


	private Label lblLabelInfo;
	private Label lblReplaceLabels;
	Button btnLabelsYes;
	Button btnLabelsNo;
	private Button btnLabelsDefault;
	
    boolean mouseOverHelpButton = false;
	
    static final Image    HELP_ICON          = new Image(Display.getDefault(), Iconify.class.getResourceAsStream("/img/28x28/help.png"));
	
	/**
	 * Filter to show or reject this section depending on input value
	 */
	public static class Filter extends ObjectFilter {
		@Override
		protected boolean isRequiredType(Object object) {
			if ( object == null )
				return false;
			
			return object instanceof IArchimateModel;
		}

		@Override
		protected Class<?> getAdaptableType() {
			return IArchimateModel.class;
		}
	}

	/**
	 * Create the controls
	 */
	@Override
	protected void createControls(Composite parent) {
		parent.setLayout(new FormLayout());

		
		/* **************************************************** */
        boolean mustReplaceIconsInTree = Iconify.INSTANCE.getPreferenceStore().getString("mustReplaceIconsInTree").length() == 0;

        this.lblIconInTreeInfo = new Label(parent, SWT.NONE);
        this.lblIconInTreeInfo.setText("Iconify");
        this.lblIconInTreeInfo.setForeground(parent.getForeground());
        this.lblIconInTreeInfo.setBackground(parent.getBackground());
        this.lblIconInTreeInfo.setFont(parent.getFont());
        
        this.lblReplaceInTreeIcons = new Label(parent, SWT.NONE);
        this.lblReplaceInTreeIcons.setText("Iconify this model:");
        this.lblReplaceInTreeIcons.setForeground(parent.getForeground());
        this.lblReplaceInTreeIcons.setBackground(parent.getBackground());
        this.lblReplaceInTreeIcons.setFont(parent.getFont());
        this.lblReplaceInTreeIcons.setEnabled(false);
        FormData fd = new FormData();
        fd.top = new FormAttachment(this.lblIconInTreeInfo, 5);
        fd.left = new FormAttachment(0, 10);
        this.lblReplaceInTreeIcons.setLayoutData(fd);

        Composite compoReplaceIconsInTree = new Composite(parent, SWT.NONE);
        compoReplaceIconsInTree.setBackground(parent.getBackground());
        compoReplaceIconsInTree.setLayout(new RowLayout(SWT.VERTICAL));
        fd = new FormData();
        fd.top = new FormAttachment(this.lblReplaceInTreeIcons, 0, SWT.TOP);
        fd.left = new FormAttachment(this.lblReplaceInTreeIcons, 20);
        compoReplaceIconsInTree.setLayoutData(fd);

        this.btnIconsInTreeYes = new Button(compoReplaceIconsInTree, SWT.RADIO);
        this.btnIconsInTreeYes.setBackground(parent.getBackground());
        this.btnIconsInTreeYes.setForeground(parent.getForeground());
        this.btnIconsInTreeYes.setFont(parent.getFont());
        this.btnIconsInTreeYes.setText("yes");
        this.btnIconsInTreeYes.setSelection(false);
        this.btnIconsInTreeYes.setEnabled(false);
        this.btnIconsInTreeYes.addSelectionListener(this.replaceIconsInTreeListener);

        this.btnIconsInTreeNo = new Button(compoReplaceIconsInTree, SWT.RADIO);
        this.btnIconsInTreeNo.setBackground(parent.getBackground());
        this.btnIconsInTreeNo.setForeground(parent.getForeground());
        this.btnIconsInTreeNo.setFont(parent.getFont());
        this.btnIconsInTreeNo.setText("no");
        this.btnIconsInTreeNo.setSelection(false);
        this.btnIconsInTreeNo.setEnabled(false);
        this.btnIconsInTreeNo.addSelectionListener(this.replaceIconsInTreeListener);

		
	}
	
	/*
     * Adapter to listen to changes made elsewhere (including Undo/Redo commands)
     */
    private Adapter eAdapter = new AdapterImpl() {
        @Override
        public void notifyChanged(Notification msg) {
            Object feature = msg.getFeature();
            // Diagram Name event (Undo/Redo and here!)
            if(feature == IArchimatePackage.Literals.PROPERTIES__PROPERTIES) {
            	refreshControls();
            }
        }
    };

	@Override
	protected Adapter getECoreAdapter() {
		return this.eAdapter;
	}

	@Override
	protected EObject getEObject() {
		return this.model;
	}

	@Override
    protected void setElement(Object element) {
		this.model = (IArchimateModel)new Filter().adaptObject(element);
		if(this.model == null) {
			logger.error("failed to get element for " + element); //$NON-NLS-1$
		}
		
		refreshControls();
	}
	
	void refreshControls() {
        boolean yes;
        boolean no;
        boolean mustReplaceIconsInViews = Iconify.INSTANCE.getPreferenceStore().getString("mustReplaceIconsInViews").length() == 0;
        boolean mustReplaceIconsInTree = Iconify.INSTANCE.getPreferenceStore().getString("mustReplaceIconsInTree").length() == 0;
        boolean mustReplaceLabelsInViews = Iconify.INSTANCE.getPreferenceStore().getString("mustReplaceLabelsInViews").length() == 0;

        if ( mustReplaceIconsInTree ) {
            this.lblIconInTreeInfo.setText("Iconify");
            String propValue = Iconify.getPropertyValue(this.model, "must replace icons in tree");
            if ( propValue != null )
                propValue = propValue.toLowerCase();
            yes = Iconify.areEqual(propValue, "yes");
            no = Iconify.areEqual(propValue, "no");

            this.btnIconsInTreeYes.setSelection(yes);
            this.btnIconsInTreeNo.setSelection(no);
        } else { 
            this.lblIconInTreeInfo.setText("Icons in model tree: the preference states to "+Iconify.INSTANCE.getPreferenceStore().getString("mustReplaceIconsInTree")+" replace icons.");
            yes = Iconify.areEqual(Iconify.INSTANCE.getPreferenceStore().getString("mustReplaceIconsInTree").toLowerCase(), "always");
            this.btnIconsInTreeYes.setSelection(yes);
            this.btnIconsInTreeNo.setSelection(!yes);
        }
        this.lblReplaceInTreeIcons.setEnabled(mustReplaceIconsInTree);
        this.btnIconsInTreeYes.setEnabled(mustReplaceIconsInTree);
        this.btnIconsInTreeNo.setEnabled(mustReplaceIconsInTree);

	}


	
	
	   private SelectionListener replaceIconsInTreeListener = new SelectionAdapter () {
	        @Override
            public void widgetSelected(SelectionEvent event) {
	            if ( SpecializationModelSection.this.model == null ) 
	                return;

	            Button button = ((Button) event.widget);
	            if ( !button.getSelection() )
	                return;
	            
	            String value = null;
	            if ( button.equals(SpecializationModelSection.this.btnIconsInTreeYes) )
	                value = "yes";
	            else if ( button.equals(SpecializationModelSection.this.btnIconsInTreeNo) )
	                value = "no";
	            
	            SpecializationPropertyCommand command = new SpecializationPropertyCommand(SpecializationModelSection.this.model, "must replace icons in tree", value);
	            
	            if ( command.canExecute() ) {
	                CompoundCommand compoundCommand = new NonNotifyingCompoundCommand();
	                compoundCommand.add(command);

	                CommandStack stack = (CommandStack) SpecializationModelSection.this.model.getArchimateModel().getAdapter(CommandStack.class);
	                stack.execute(compoundCommand);
	                logger.trace("Setting property \"must replace icons in tree\" to "+value);
	                Iconify.refreshIconsAndLabels(SpecializationModelSection.this.model);
	            }
	        }
	    };

	private SelectionListener replaceLabelsListener = new SelectionAdapter () {
		@Override
        public void widgetSelected(SelectionEvent event) {
			if ( SpecializationModelSection.this.model == null ) 
				return;

			Button button = ((Button) event.widget);
			if ( !button.getSelection() )
				return;

			String value = null;
			if ( button.equals(SpecializationModelSection.this.btnLabelsYes) )
				value = "yes";
			else if ( button.equals(SpecializationModelSection.this.btnLabelsNo) )
				value = "no";
			
			SpecializationPropertyCommand command = new SpecializationPropertyCommand(SpecializationModelSection.this.model, "must replace labels", value);
			
            if ( command.canExecute() ) {
    			CompoundCommand compoundCommand = new NonNotifyingCompoundCommand();
            	compoundCommand.add(command);

    		    CommandStack stack = (CommandStack) SpecializationModelSection.this.model.getAdapter(CommandStack.class);
    		    stack.execute(compoundCommand);
    		    logger.trace("Setting property \"must replace labels\" to "+value);
    		    Iconify.refreshIconsAndLabels(SpecializationModelSection.this.model);
            }
		}
	};
}
